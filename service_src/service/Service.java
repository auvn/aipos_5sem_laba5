package com.auvn.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.auvn.service.objects.Article;
import com.auvn.service.objects.Category;

@Path("/networks_manual_service")
public class Service implements Serializable {

	private File dbFile = new File("db" + "2");
	private Category rootCategory;
	private ArrayList<String> tempList;
	public static final String SEPARATOR = "?";

	public Service() {
		try {
			init();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@PUT
	@Path("/{categoryPath}/{subCategoryName}/addCategory")
	public void addCategory(@PathParam("categoryPath") String categoryPath,
			@PathParam("subCategoryName") String subCategoryName)
			throws FileNotFoundException, IOException {
		Category categoryForNew = rootCategory;
		rootCategory.setLastStatus("Ok");
		if (!categoryPath.equals("")) {
			categoryForNew = rootCategory.findCategory(
					categoryPath.split("[" + SEPARATOR + "]"), 0);
			for (Category subCategory : categoryForNew.getSubCategories()) {
				if (subCategory.getName().equals(subCategoryName))
					rootCategory.setLastStatus("Fail");
			}
		}
		if (rootCategory.getLastStatus().equals("Ok"))
			categoryForNew.addCategory(subCategoryName);
		flushDB();

	}

	@DELETE
	@Path("/{categoryPath}/{categoryName}/removeCategory")
	public void removeCategory(@PathParam("categoryPath") String categoryPath,
			@PathParam("categoryName") String categoryName)
			throws FileNotFoundException, IOException {
		if (!categoryPath.equals("")) {
			Category categoryForRemove = rootCategory.findCategory(
					categoryPath.split("[" + SEPARATOR + "]"), 0);
			System.out.println("TEST " + categoryForRemove.getName());
			for (Category subCategory : categoryForRemove.getSubCategories()) {
				if (subCategory.getName().equals(categoryName)) {
					categoryForRemove.removeCategory(subCategory);
					flushDB();
					rootCategory.setLastStatus("Ok");
				}
			}
		}
		rootCategory.setLastStatus("Fail");
	}

	@PUT
	@Path("/{oldCategoryPath}/{newCategoryName}/editCategory")
	public void editCategory(
			@PathParam("oldCategoryPath") String oldCategoryPath,
			@PathParam("newCategoryName") String newCategoryName)
			throws FileNotFoundException, IOException {
		System.out.println(oldCategoryPath);
		if (!oldCategoryPath.equals(SEPARATOR)) {
			Category categoryForEdit = rootCategory.findCategory(
					oldCategoryPath.split("[" + SEPARATOR + "]"), 0);
			if (categoryForEdit.getName().length() != 0) {
				categoryForEdit.setName(newCategoryName);
				flushDB();
				rootCategory.setLastStatus("Ok");
			}
		}
		rootCategory.setLastStatus("Fail");
	}

	@GET
	@Produces(MediaType.APPLICATION_XML)
	@Path("/{categoryPath}/getSubCategories")
	public String getSubCategories(
			@PathParam("categoryPath") String categoryName) {
		List<Category> categories = rootCategory.findCategory(
				categoryName.split("[" + SEPARATOR + "]"), 0)
				.getSubCategories();
		System.out.println(categories.size());
		StringBuffer response = new StringBuffer("<categories>");

		for (Category category : categories) {
			response.append("<category>");
			response.append(category.getName());
			response.append("</category>");
		}
		response.append("</categories>");
		return response.toString();
	}

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/getRootCategoryName")
	public String getRootCategoryName() {
		return rootCategory.getName();
	}

	@POST
	@Path("/{articlePath}/{newArticleName}/addArticle")
	public void addArticle(@PathParam("articlePath") String articlePath,
			@PathParam("newArticleName") String newArticleName,
			String newArticleData) throws FileNotFoundException, IOException {
		Category category = rootCategory.findCategory(
				articlePath.split("[" + SEPARATOR + "]"), 0);
		if (category.getName() != null) {
			if (category.findArticle(newArticleName).getName() == null) {
				category.addArticle(new Article(newArticleName, newArticleData));
				rootCategory.setLastStatus("Ok");
				flushDB();

			}
		}
		rootCategory.setLastStatus("Fail");
	}

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/getStatus")
	public String getStatus() {
		return rootCategory.getLastStatus();
	}

	@DELETE
	@Path("/{articlePath}/{articleName}/removeArticle")
	public void removeArticle(@PathParam("articlePath") String articlePath,
			@PathParam("articleName") String articleName)
			throws FileNotFoundException, IOException {
		Category category = rootCategory.findCategory(
				articlePath.split("[" + SEPARATOR + "]"), 0);
		Article oldArticle = category.findArticle(articleName);
		if (oldArticle.getName() != null) {
			category.removeArticle(oldArticle);
			flushDB();
			rootCategory.setLastStatus("Ok");
		}
		rootCategory.setLastStatus("Fail");
	}

	@GET
	@Produces(MediaType.APPLICATION_XML)
	@Path("/{articlePath}/{articleName}/getArticleInfo")
	public String getArticleInfo(@PathParam("articlePath") String articlePath,
			@PathParam("articleName") String articleName) {
		Article article = rootCategory.findCategory(
				articlePath.split("[" + SEPARATOR + "]"), 0).findArticle(
				articleName);
		StringBuffer response = new StringBuffer("<article>");
		if (article.getName() != null) {
			response.append("<name>" + article.getName() + "</name>");
			response.append("<data>" + article.getData() + "</data>");
		}
		response.append("</article>");
		return response.toString();
	}

	@POST
	@Path("/{articlePath}/{oldArticleName}/{newArticleName}/editArticle")
	public void editArticle(@PathParam("articlePath") String articlePath,
			@PathParam("oldArticleName") String oldArticleName,
			@PathParam("newArticleName") String newArticleName,
			String newArticleData) throws FileNotFoundException, IOException {
		Article oldArticle = rootCategory.findCategory(
				articlePath.split("[" + SEPARATOR + "]"), 0).findArticle(
				oldArticleName);
		if (oldArticle.getName() != null) {
			System.out.println("Name: " + newArticleName);
			oldArticle.setName(newArticleName);
			System.out.println("Data: " + newArticleData);
			oldArticle.setData(newArticleData);
			flushDB();
			rootCategory.setLastStatus("Ok");
		}
		rootCategory.setLastStatus("Fail");
	}

	@GET
	@Produces(MediaType.APPLICATION_XML)
	@Path("/{categoryPath}/getArticles")
	public String getArticles(@PathParam("categoryPath") String categoryPath) {
		tempList.clear();
		List<Article> articles = rootCategory.findCategory(
				categoryPath.split("[" + SEPARATOR + "]"), 0).getArticles();
		StringBuffer response = new StringBuffer("<articles>");

		for (Article article : articles) {
			response.append("<article>");
			response.append(article.getName());
			response.append("</article>");
		}
		response.append("</articles>");
		return response.toString();
	}

	private void flushDB() throws FileNotFoundException, IOException {
		ObjectOutputStream dbFileObjectOutputStream = new ObjectOutputStream(
				new FileOutputStream(dbFile));
		dbFileObjectOutputStream.writeObject(rootCategory);
		dbFileObjectOutputStream.flush();
		dbFileObjectOutputStream.close();
	}

	public void init() throws IOException, ClassNotFoundException {
		System.out.println("new client");
		if (!dbFile.exists()) {

			rootCategory = new Category(SEPARATOR);
			rootCategory.addCategory("new");
			flushDB();
			System.out.println("db file created");
		} else {
			ObjectInputStream dbFileObjectInputStream = new ObjectInputStream(
					new FileInputStream(dbFile));
			rootCategory = (Category) dbFileObjectInputStream.readObject();
			dbFileObjectInputStream.close();
		}

		tempList = new ArrayList<String>();
	}

	public Category getRootCategory() {
		return rootCategory;
	}
}
