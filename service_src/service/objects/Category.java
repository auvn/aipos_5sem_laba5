package com.auvn.service.objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Category implements Serializable {
	public static final long serialVersionUID = -1475386750518229841L;
	private List<Article> articles;
	private List<Category> subCategories;
	private String name;
	private String lastStatus = "Fail";
	private int articleId = 0;

	public Category() {
		articles = new ArrayList<Article>();
		subCategories = new ArrayList<Category>();
	}

	public Category(String categoryName) {
		this();
		this.name = categoryName;
	}

	public Category findCategory(String[] categoriesName, int level) {
		if (level == categoriesName.length) {
			return this;
		}
		for (Category category : getSubCategories()) {
			if (category.getName().equals(categoriesName[level]))
				return category.findCategory(categoriesName, level + 1);
		}
		return new Category(null);
	}

	public Article findArticle(String articleName) {
		for (Article tempArticle : articles) {
			if (tempArticle.getName().equals(articleName))
				return tempArticle;
		}
		return new Article(null, null);
	}

	public void addCategory(String categoryName) {
		subCategories.add(new Category(categoryName));
	}

	public void removeCategory(Category category) {
		subCategories.remove(category);
	}

	public void renameCategory(String newCategoryName) {
		setName(newCategoryName);
	}

	public void addArticle(Article article) {
		article.setId(++articleId);
		articles.add(article);
	}

	public void removeArticle(Article article) {
		articles.remove(article);
	}

	public void replaceArticle(int articleId, Article article) {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		System.out.println("test");
		this.name = name;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public List<Category> getSubCategories() {
		return subCategories;
	}

	public String getLastStatus() {
		return lastStatus;
	}

	public void setLastStatus(String lastStatus) {
		this.lastStatus = lastStatus;
	}

}
