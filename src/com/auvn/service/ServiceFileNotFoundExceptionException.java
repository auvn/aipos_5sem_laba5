
/**
 * ServiceFileNotFoundExceptionException.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */

package com.auvn.service;

public class ServiceFileNotFoundExceptionException extends java.lang.Exception{

    private static final long serialVersionUID = 1352510164053L;
    
    private com.auvn.service.ServiceStub.ServiceFileNotFoundException faultMessage;

    
        public ServiceFileNotFoundExceptionException() {
            super("ServiceFileNotFoundExceptionException");
        }

        public ServiceFileNotFoundExceptionException(java.lang.String s) {
           super(s);
        }

        public ServiceFileNotFoundExceptionException(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public ServiceFileNotFoundExceptionException(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(com.auvn.service.ServiceStub.ServiceFileNotFoundException msg){
       faultMessage = msg;
    }
    
    public com.auvn.service.ServiceStub.ServiceFileNotFoundException getFaultMessage(){
       return faultMessage;
    }
}
    