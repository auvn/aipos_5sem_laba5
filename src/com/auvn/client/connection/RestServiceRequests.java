package com.auvn.client.connection;

import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.thoughtworks.xstream.XStream;

public class RestServiceRequests implements ServiceRequests {
	private WebResource webResource;
	private String response;

	public RestServiceRequests() {
		webResource = Client.create(new DefaultClientConfig())
				.resource("http://localhost:8080/aipos_5sem_laba5_rest")
				.path("resources").path("networks_manual_service");
	}

	public String getRootCategoryName() {
		return webResource.path(Procedures.GET_ROOT_CATEGORY_NAME)
				.accept(MediaType.TEXT_PLAIN).get(String.class)
				.replace('?', '/');
	}

	public String[] getSubCategories(String categoryPath) {
		response = webResource
				.path(categoryPath.replace('/', '?') + "/"
						+ Procedures.GET_SUB_CATEGORIES)
				.accept(MediaType.APPLICATION_XML).get(String.class);
		XStream xStream = new XStream();
		xStream.alias("category", String.class);
		xStream.alias("categories", String[].class);
		return (String[]) xStream.fromXML(response);
	}

	public String[] getArticles(String categoryPath) {
		response = webResource
				.path(categoryPath.replace('/', '?') + "/"
						+ Procedures.GET_ARTICLES)
				.accept(MediaType.APPLICATION_XML).get(String.class);
		XStream xStream = new XStream();
		xStream.alias("article", String.class);
		xStream.alias("articles", String[].class);
		return (String[]) xStream.fromXML(response);
	}

	public String addArticle(String articlePath, String newArticleName,
			String newArticleData) {
		webResource.path(
				articlePath.replace('/', '?') + "/"
						+ newArticleName.replace('/', '|') + "/"
						+ Procedures.ADD_ARTICLE).post(newArticleData);
		return webResource.path(Procedures.GET_STATUS)
				.accept(MediaType.TEXT_PLAIN).get(String.class);
	}

	public String[] getArticleInfo(String articlePath, String articleName) {
		response = webResource
				.path(articlePath.replace('/', '?') + "/"
						+ articleName.replace('/', '|') + "/"
						+ Procedures.GET_ARTICLE_INFO)
				.accept(MediaType.APPLICATION_XML).get(String.class);
		XStream xStream = new XStream();
		xStream.alias("name", String.class);
		xStream.alias("data", String.class);
		xStream.alias("article", String[].class);
		return (String[]) xStream.fromXML(response);
	}

	public String removeArticle(String articlePath, String articleName) {
		webResource.path(
				articlePath.replace('/', '?') + "/"
						+ articleName.replace('/', '|') + "/"
						+ Procedures.REMOVE_ARTICLE).delete();
		return webResource.path(Procedures.GET_STATUS)
				.accept(MediaType.TEXT_PLAIN).get(String.class);
	}

	public String editArticle(String articlePath, String oldArticleName,
			String newArticleName, String newArticleData) {
		webResource.path(
				articlePath.replace('/', '?') + "/"
						+ oldArticleName.replace('/', '|') + "/"
						+ newArticleName.replace('/', '|') + "/"
						+ Procedures.EDIT_ARTICLE).post(newArticleData);
		return webResource.path(Procedures.GET_STATUS)
				.accept(MediaType.TEXT_PLAIN).get(String.class);
	}

	public String addCategory(String categoryPath, String subCategoryName) {
		webResource.path(
				categoryPath.replace('/', '?') + "/"
						+ subCategoryName.replace('/', '|') + "/"
						+ Procedures.ADD_CATEGORY).put();
		return webResource.path(Procedures.GET_STATUS)
				.accept(MediaType.TEXT_PLAIN).get(String.class);
	}

	public String removeCategory(String categoryPath, String categoryName) {
		webResource.path(
				categoryPath.replace('/', '?') + "/"
						+ categoryName.replace('/', '|') + "/"
						+ Procedures.REMOVE_CATEGORY).delete();
		return webResource.path(Procedures.GET_STATUS)
				.accept(MediaType.TEXT_PLAIN).get(String.class);
	}

	public String editCategory(String oldCategoryPath, String newCategoryName) {
		webResource.path(
				oldCategoryPath.replace('/', '?') + "/"
						+ newCategoryName.replace('/', '|') + "/"
						+ Procedures.EDIT_CATEGORY).put();
		return webResource.path(Procedures.GET_STATUS)
				.accept(MediaType.TEXT_PLAIN).get(String.class);
	}

}
