package com.auvn.client.connection;

import org.apache.axis.AxisFault;

public class RpcServiceRequests implements ServiceRequests {
	private ServiceConnection serviceConnection;

	public RpcServiceRequests(ServiceConnection serviceConnection) {
		this.serviceConnection = serviceConnection;
	}

	public String getRootCategoryName() {
		try {
			return (String) serviceConnection.callProcedure(
					Procedures.GET_ROOT_CATEGORY_NAME, null);
		} catch (AxisFault e) {
			e.printStackTrace();
		}
		return null;
	}

	public String[] getSubCategories(String categoryPath) {
		try {
			return (String[]) serviceConnection.callProcedure(
					Procedures.GET_SUB_CATEGORIES,
					new Object[] { categoryPath });
		} catch (AxisFault e) {
			e.printStackTrace();
		}
		return null;
	}

	public String[] getArticles(String categoryPath) {
		try {
			return (String[]) serviceConnection.callProcedure(
					Procedures.GET_ARTICLES, new Object[] { categoryPath });
		} catch (AxisFault e) {
			e.printStackTrace();
		}
		return null;
	}

	public String addArticle(String articlePath, String newArticleName,
			String newArticleData) {
		try {
			return (String) serviceConnection.callProcedure(
					Procedures.ADD_ARTICLE, new Object[] { articlePath,
							newArticleName, newArticleData });
		} catch (AxisFault e) {
			e.printStackTrace();
		}
		return null;
	}

	public String[] getArticleInfo(String articlePath, String articleName) {
		try {
			return (String[]) serviceConnection.callProcedure(
					Procedures.GET_ARTICLE_INFO, new Object[] { articlePath,
							articleName });
		} catch (AxisFault e) {
			e.printStackTrace();
		}
		return null;
	}

	public String removeArticle(String articlePath, String articleName) {
		try {
			return (String) serviceConnection.callProcedure(
					Procedures.REMOVE_ARTICLE, new Object[] { articlePath,
							articleName });
		} catch (AxisFault e) {
			e.printStackTrace();
		}
		return null;
	}

	public String editArticle(String articlePath, String oldArticleName,
			String newArticleName, String newArticleData) {
		try {
			return (String) serviceConnection.callProcedure(
					Procedures.EDIT_ARTICLE, new Object[] { articlePath,
							oldArticleName, newArticleName, newArticleData });
		} catch (AxisFault e) {
			e.printStackTrace();
		}
		return null;
	}

	public String addCategory(String categoryPath, String subCategoryName) {
		try {
			return (String) serviceConnection.callProcedure(
					Procedures.ADD_CATEGORY, new Object[] { categoryPath,
							subCategoryName });
		} catch (AxisFault e) {
			e.printStackTrace();
		}
		return null;
	}

	public String removeCategory(String categoryPath, String categoryName) {
		try {
			return (String) serviceConnection.callProcedure(
					Procedures.REMOVE_CATEGORY, new Object[] { categoryPath,
							categoryName });
		} catch (AxisFault e) {
			e.printStackTrace();
		}
		return null;
	}

	public String editCategory(String oldCategoryPath, String newCategoryName) {
		try {
			return (String) serviceConnection.callProcedure(
					Procedures.EDIT_CATEGORY, new Object[] { oldCategoryPath,
							newCategoryName });
		} catch (AxisFault e) {
			e.printStackTrace();
		}
		return null;
	}
}
