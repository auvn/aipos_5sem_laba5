package com.auvn.client.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import com.auvn.client.gui.MainFrame;

public class ArticlesButtonsActions implements ActionListener {
	private MainFrame mainFrame;
	private JButton pressedButton;
	private ArticlesListActions articlesListActions;

	public ArticlesButtonsActions(MainFrame mainFrame,
			ArticlesListActions articlesListActions) {
		this.mainFrame = mainFrame;
		this.articlesListActions = articlesListActions;
	}

	public void actionPerformed(ActionEvent e) {
		pressedButton = (JButton) e.getSource();
		if (pressedButton == mainFrame.getMainPanel().getAddArticle()) {
			articlesListActions.showAddDialog();
		} else if (pressedButton == mainFrame.getMainPanel().getEditArticle()) {
			articlesListActions.showEditDialog();
		}

	}
}
