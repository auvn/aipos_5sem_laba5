package com.auvn.client.gui;

import javax.swing.JFrame;

import com.auvn.client.gui.panels.MainPanel;

public class MainFrame extends JFrame {
	private MainPanel mainPanel;

	public MainFrame() {
		setProperties();
		if (mainPanel == null)
			mainPanel = new MainPanel();

		setContentPane(mainPanel);

	}

	private void setProperties() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(500, 500);
	}

	public MainPanel getMainPanel() {
		return mainPanel;
	}
	
}
